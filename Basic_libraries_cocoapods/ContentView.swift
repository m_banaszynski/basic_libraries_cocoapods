//
//  ContentView.swift
//  Basic_libraries_cocoapods
//
//  Created by Maciej Banaszyński on 07/07/2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

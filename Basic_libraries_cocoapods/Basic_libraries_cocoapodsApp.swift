//
//  Basic_libraries_cocoapodsApp.swift
//  Basic_libraries_cocoapods
//
//  Created by Maciej Banaszyński on 07/07/2021.
//

import SwiftUI

@main
struct Basic_libraries_cocoapodsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
